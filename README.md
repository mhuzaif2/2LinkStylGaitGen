2LinkStylGaitGen:
----------------

The goal of this work is to use three link planar biped model in generating variable gaits. The gait parameters used are:

TL - The step length of the planar biped model
J (cost) - Selected from one of {q_{sw}(t),(dq_{sw}(t))2, 100, ||u(t)||2}

Run by playing GUI_interface.m and selecting the desired values for above variables.