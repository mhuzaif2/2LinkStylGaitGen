function cost = costFun_GUI(t, x, u, p)
% This function computes the Path Constraints in optimization

% INPUTS:
%   t = time vector for the trajectory
%   x = state vector for the trajectory
%   u = input vector for the trajectory
%   p = parameters for the model

% OUTPUTS:
%   cost = computed cost for the trajectory

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   switch p.cost

      case 'minE'
      %%%%%% Minimum Energy

      cost = u(1,:).^2;

      case 'const'
      %%%%%% Constant Cost

      cost = 100 * ones(1, size(t,2));
      
      case 'qsw'
      %%%%%% Swing Leg Angle
      
      cost =  x(2,:);
      
      case 'qsw2'
      %%%%%% Swing Leg Angle (with squared norm)
      
      cost =  x(2,:).^2;
      
      case 'dqsw2'
      %%%%%% Swing Leg Angular Velocity (with squared norm)
      
      cost =  x(4,:).^2;
      
      case 'com'
      %%%%%% Vertical Position of Center of Mass
          
      t = pos_com(x(1,:), x(2,:), p.m, p.r);
      cost = t(2,:);

  end


end
