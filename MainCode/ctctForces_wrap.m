function [Fx,Fy] = ctctForces_wrap(q_st,q_sw,dq_st,dq_sw,ddq_st,ddq_sw,m,r,g)
%AUTOGEN_CONTACTFORCE
%    [FX,FY] = AUTOGEN_CONTACTFORCE(Q_ST,Q_SW,DQ_ST,DQ_SW,DDQ_ST,DDQ_SW,M,R,G)

%    This function was generated by the Symbolic Math Toolbox version 7.1.
%    13-Jun-2018 11:56:27

t2 = sin(q_st);
t3 = sin(q_sw);
t4 = dq_st.^2;
t5 = cos(q_st);
t6 = dq_sw.^2;
t7 = cos(q_sw);
Fx = m.*(ddq_st.*r.*t5.*3.0-ddq_sw.*r.*t7-r.*t2.*t4.*3.0+r.*t3.*t6).*(1.0./2.0);
if nargout > 1
    Fy = g.*m.*2.0-ddq_st.*m.*r.*t2.*(3.0./2.0)+ddq_sw.*m.*r.*t3.*(1.0./2.0)-m.*r.*t4.*t5.*(3.0./2.0)+m.*r.*t6.*t7.*(1.0./2.0);
end
