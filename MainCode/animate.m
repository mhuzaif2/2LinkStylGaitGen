% animate.m
% Generates animation of the biped model for a given number of steps, and
% sampling frequency for display

% Updates with respect to the simulations:

% - Refined the old animation code
% - Removed the ticks in the axis
% - Biped animated as walking on ground not on tread mill for n number of
% steps


% Umer Huzaifa
% RAD Lab UIUC
% 14th Jan 2018

function animate(options, Fstep, nstep)

%     Fstep  -- Sampling rate per step
%     nstep  -- Number of steps in animation

    load('data.mat');
    
    nTime = size(x,2);

    switch nargin
        case 0
          close all          
          options={'ColorScheme',1,'Disp',1}; % Both legs in grey color,
                                              % Animate one step with shots
                                              % placed horizontally          
          if (max(t)==1) 
              Fstep = 20;
          elseif(max(t)==2)
              Fstep = 10;
          end
          
          nstep = 5;
    end
    anim(t,x,Fstep,nstep,options)


function anim(t,x,Fs,nstep,options)

  [n,m] = size(x');
  [vV,vH] = hip_vel(x'); % convert angles to horizontal position of hips
  v = [vH vV];
  pH_horiz = zeros(n,1);
  pH = zeros(n,2);

	% Estimate hip position by estimating integral of hip velocity
  for j=2:n
     pH(j,:) = pH(j-1,:)+(t(j)-t(j-1))*v(j-1,:);
  end

  pH_horiz = pH(:,1); % Take out the horizontal component of the hip position
  [te,pH_horiz] = even_sample(t',pH_horiz,Fs);
  [te,xe] = even_sample(t',x',Fs);
  [n,m] = size(xe);

  % Deciding the starting position for the feet and the hip
  offset = [0;0];
  q = x(1:2,1);
  [pFoot1,pFoot2,pH] = limb_position(q, offset);

  cla        % Clear current axis.
  anim_axis = [-1 4.5 0 4];
  axis(anim_axis)
  set(gca,'xtick',[])
  set(gca,'ycolor','None')

	% Use actual relations between masses in animation
  [r,m,L,g] = model_params_two_link;
  scl = 0.04; % factor to scale masses
  mr_legs = m^(1/3)*scl; % radius of mass for legs
  ground_color = 'k'; % a.k.a. black

	% Approximate circular shape of mass
  param = linspace(0,2*pi+2*pi/50,50);
  xmass_legs = mr_legs*cos(param); % Definition of the circle
  ymass_legs = mr_legs*sin(param); % representing legs COM

    % Color Choices for Legs
  switch options{2}
        case 1
            leg1_color = [0.5,0.5,0.5];
            leg2_color = 0.2 * [1,1,1];
        case 2
            leg1_color = [0,0,1];
            leg2_color = [1,0,0];
  end
	% Draw ground
	buffer = 5;
	ground = line([-buffer pH_horiz(n)+buffer],[0 0]);
	set(ground,'Color',ground_color,'LineWidth',2);


	% Draw leg one
  leg1 = line([pFoot1(1) pH(1)],[pFoot1(2) pH(2)]);
  mass1 = patch(xmass_legs+(pH(1)-pFoot1(1))/2,...
            ymass_legs+(pH(2)-pFoot1(2))/2,leg1_color);
  set(mass1,'EdgeColor',leg1_color)
  set(leg1,'LineWidth',2,'Color',leg1_color);

	% Draw leg two
 
  leg2 = line([pFoot2(1) pH(1)],[pFoot2(2) pH(2)]);
  mass2 = patch(xmass_legs+pH(1)-(pH(1)-pFoot2(1))/2,...
           ymass_legs+pH(2)-(pH(2)-pFoot2(2))/2,leg2_color);
  set(mass2,'EdgeColor',leg2_color)
  set(leg2,'LineWidth',2,'Color',leg2_color);


  nex_step = [0;0];
        for kstep = 1: nstep
            for k=2:n
                offset = nex_step;
                q = xe(k,1:2);
                [pFoot1,pFoot2,pH] = limb_position(q, offset);
                set(leg1,'XData',[pFoot1(1) pH(1)],'YData',[pFoot1(2) pH(2)]);
                set(mass1,'XData',xmass_legs + (pH(1)-pFoot1(1))/2 + offset(1),...
                    'YData',ymass_legs+(pH(2)-pFoot1(2))/2)+offset(2);
                set(leg2,'XData',[pFoot2(1) pH(1)],'YData',[pFoot2(2) pH(2)]);
                set(mass2,'XData',xmass_legs+pH(1)-(pH(1)-pFoot2(1))/2,...
                    'YData',ymass_legs+pH(2)-(pH(2)-pFoot2(2))/2);
                drawnow;
                pause(1/20);
            end            
            nex_step = pFoot2;
            swap(leg1, leg2);
            swap(mass1, mass2);
        end

  %%% --------------------------------------------------------------------------
  %%% a function to calculate hip velocity
  function [vV,vH] = hip_vel(x)

   [r,m,L,g]=model_params_two_link;
  	vV = zeros(length(x),1);
  	vH = r* cos(x(:,1)).*x(:,3); % estimate of horizontal velocity of hips
    vV = -r* sin(x(:,1)).*x(:,3);

  %%% --------------------------------------------------------------------------
  %%% a function to calculate the limb position
  function [pFoot1,pFoot2,pH] = limb_position(q, offset)
    % Use position of hips as location of stance leg foot.
  	[r,m,L,g]=model_params_two_link;
  	pFoot1=[0; 0] + offset;
  	pH=[pFoot1(1)+r*sin(q(1)); pFoot1(2)+r*cos(q(1))];
  	pFoot2=[pH(1)-r*sin(q(2)); pH(2)-r*cos(q(2))];


  %%% --------------------------------------------------------------------------
  %%% CONVERTS A RANDOMLY SAMPLED SIGNAL SET INTO AN EVENLY SAMPLED SIGNAL SET
  %%% (by interpolation)
  %%%
  %%% written by Haldun Komsuoglu, 7/23/1999
  function [Et, Ex] = even_sample(t, x, Fs)
  	% Obtain the process related parameters
  	N = size(x, 2);    % number of signals to be interpolated
  	M = size(t, 1);    % Number of samples provided
  	t0 = t(1,1);       % Initial time
  	tf = t(M,1);       % Final time
  	EM = (tf-t0)*Fs;   % Number of samples in the evenly sampled case with
  	% the specified sampling frequency
  	Et = linspace(t0, tf, EM)';
  	% Using linear interpolation (used to be cubic spline interpolation)
  	% and re-sample each signal to obtain the evenly sampled forms
  	for s = 1:N,
        Ex(:,s) = interp1(t(:,1), x(:,s), Et(:,1));
    end
