% optim_dcol.m

% A matlab script for finding a stable gait for a 2 link biped model using
% trajectory optimization.

% Umer Huzaifa
% RAD Lab UIUC
% July 15th 2017


function optim_dcol(gait_param,handles)
  clc
  dbstop if error
  addpath ../essentials

  switch nargin
        case 0
            close all
            gait_param.TL = 0.4;             
            % (Note: Number of samples (sT * Fs) should be the same (let's say 20) for 
            % comparing multiple gaits with each other)                       
            gait_param.sT = 2; gait_param.Fs = 20; gait_param.nstep = 5;
            gait_param.cost = 'dqsw2'; % pick between 'const' or 'minE'            
      otherwise
  end

  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
  %                  Parameters for the dynamics function                   %
  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
  [r,m,L,g] = model_params_two_link;
  param.m = m;
  param.g = g;
  param.L = L;
  param.r = r;
  param.stepTime = gait_param.sT;  
  param.stepLength = gait_param.TL;
  param.cost = gait_param.cost;
  param.mu = .6;        % Friction Co-efficient
  param.knobs = gait_param;
  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
  %                       Set up function handles                           %
  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

  problem.func.dynamics = @(t, x, u)( dynamics(x,u) );
  problem.func.pathObj = @(t, x, u)( costFun_GUI(t,x,u, param) );
  
  problem.func.bndCst = @(t0, x0, tF, xF)( periodicGait_GUI(x0,xF,param) ); 
  problem.func.pathCst = @(t, x, u)( pathConstraint_GUI(t, x, u, param) );

  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
  %               Set up bounds on time, state, and control                 %
  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

  problem.bounds.initialTime.low = 0;
  problem.bounds.initialTime.upp = 0;
  problem.bounds.finalTime.low = param.stepTime;
  problem.bounds.finalTime.upp = param.stepTime;


  qLow = (-pi/2)*ones(2,1);
  qUpp = -qLow;
  dqLow = -5 * ones(2,1); %
  dqUpp = -dqLow;

  problem.bounds.state.low = [qLow; dqLow];
  problem.bounds.state.upp = [qUpp; dqUpp];
  problem.bounds.initialstate.low = [qLow; dqLow];
  problem.bounds.initialstate.upp = [qUpp; dqUpp];
  problem.bounds.finalstate.low = [qLow; dqLow];
  problem.bounds.finalstate.upp = [qUpp; dqUpp];

  uMax = 100;  %Nm
  problem.bounds.control.low = -uMax*ones(1,1);
  problem.bounds.control.upp = uMax*ones(1,1);

  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
  %              Problem Setup                 %
  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

  % For now, just assume a linear trajectory between boundary values

  problem.guess.time = [0, param.stepTime];

  % Guess for the state vector
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%

  q0 = [-0.16721      0.34482 ]';
  qF = [0.34482     -0.16721]';   %Flip stance-swing
  dq0 = [ 1.4491       0.5288 ]';
  dqF = [1.6595      -3.2482  ]';

  problem.guess.state = [ q0, qF; dq0, dqF ];
  problem.guess.control = zeros(1,2);  % Start with passive trajectory
  problem.options.method = 'gpops';  % Other options include {'trapezoid', ...
                                     % 'hermiteSimpson', 'chebyshev', 'rungeKutta'}
  problem.options.defaultAccuracy = 'medium';

  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
  %                           Solve!                                        %
  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

  %%%%% THE KEY LINE:
  tic
  soln = optimTraj(problem);
  toc
  size(soln.grid.time,2)
  
  % Transcription Grid points:
  t = soln(end).grid.time;
  q = soln(end).grid.state(1:2,:);
  dq = soln(end).grid.state(3:4,:);
  u = soln(end).grid.control;
  x = [q;dq];

  [Fx_final, Fy_final] = contactForces(t,x,u,param);

  disp('Minimum value of Normal GRF on Swing Foot is: \n')
  min(Fy_final)

  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
  %                     Analyze the Solution                                %
  %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%

  xF = [q(:,end); dq(:,end)];
  swingFoot_pos(xF(1:2),param)
  x0_model = transition_two_link(xF');

  disp('Errors between first initial position and next is: ')

  % periodicity constraint check -- solutions are not good if these values
  % are not small
  ceqPos = x0_model(1:2) - q(:,1)'
  ceqVel = x0_model(3:4) - dq(:,1)'

  save('data.mat','x','t','u','param') % save data for animation
  
  F = [Fx_final Fy_final];
  
  if ~nargin ==0
     plot_data_2link(t, q, dq, u, F, handles);
  else
     plot_data_2link(t, q, dq, u, F)
  end
    
  if nargin==0          
    pos = [245.2857 2.9333 115.7143 44.9333];
      figure('Color','White','Units','characters','Position',pos);      
      subplot(211)      
      animate({'ColorScheme',2}, gait_param.Fs, gait_param.nstep)       
      subplot(212)
      motion_shots_2link({'ColorScheme',2}, gait_param.Fs)
  else
      axes(handles.axes13)
      hold on       
      animate({'ColorScheme',2}, gait_param.Fs, gait_param.nstep)  
      axes(handles.axes21)       
      motion_shots_2link({'ColorScheme',2}, gait_param.Fs)
  end
 
