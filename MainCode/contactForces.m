function [Fx, Fy] = ctctForces_wrap(t, x, u, p)
% [Fx, Fy] = contactForces(q,dq,ddq,p)
%
% This function computes the contact forces for the five-link biped
%
% INPUTS:
%   q = [5,n] = configuration
%   dq = [5,n] = rates
%   ddq = [5,n] = accelerations
%   p = parameter struct
%
% OUTPUTS:
%   Fx = [1,n] = horizontal contact force acting on robot
%   Fy = [1,n] = vertical contact force acting on robot

    s = size(x, 2);
    force = [];
    [r,m,L,g]=model_params_two_link;
    dx = dynamics(x, u);
    for i = 1:s                    
        % Based on the force balance of the system
        [f_t, f_n] = ctctForces_wrap (x(1,i),x(2,i),dx(1,i),dx(2,i),dx(3,i),dx(4,i),m,r,g);   
        force = [force; f_t, f_n];
    end 
    Fx = force(:,1);
    Fy = force(:,2);
end


