function [c, ceq] = periodicGait_GUI(x0,xF,p)
% This function computes End Point Constraints in optimization

% INPUTS:
%   x0 = state at the start of the trajectory
%   xF = state at the end of the trajectory

% OUTPUTS:
%   ceq = equality constraint
%   ceqGrad = gradient of equality constraints


    swF = swingFoot_pos(xF(1:2),p);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Periodicity Constraint
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ceq1 = heelStrike(x0,xF);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Terminal Footpoint Position Constraint
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Step Length Constraint

    ceq2 = swF(1) - p.stepLength;

% Vertical Position Constraint
            
    ceq3 = swF(2); % Height of the swing foot at the end should be zero                       
                                      
    % Pack up equality and inequality constraints:
    ceq4 = [];
    ceq = [ceq1; ceq2; ceq3; ceq4];        
    c = [];    
end

function p2f = swingFoot_pos( qF ,p)
% This function computes swing foot position

% INPUTS:
%   qF = final joint position
%   p = parameter vector

% OUTPUTS:
%   p2f = position vector of the swing foot

     q_st = qF(1); q_sw = qF(2);

     p_Mh = p.r * [sin(q_st); cos(q_st)];

     p2f = p_Mh - p.r *[sin(q_sw);cos(q_sw)];
 
end



function [ceq, ceqGrad] = heelStrike(x0,xF)
% This function computes the periodic step condition at the end of each
% heel strike

% INPUTS:
%   x0 = state at the start of the trajectory
%   xF = state at the end of the trajectory

% OUTPUTS:
%   ceq = equality constraint
%   ceqGrad = gradient of equality constraints

    qF = xF(1:2);
    q0 = x0(1:2);
    dqF = xF(3:4);
    dq0 = x0(3:4);

    x0_model = transition_two_link(xF');

    ceqPos = x0_model(1:2)' - q0;
    ceqVel = x0_model(3:4)' - dq0;
    ceq = [ceqPos;ceqVel];

end
