function [x_new,F_ext]=transition_two_link(x)
% TRANSITION_TWO_LINK    Calculate the state of the system after impact.
%          (Last two entries are the forces at the toe.
%    [x_new, F_ext] = TRANSITION_TWO_LINK(X) is the transition function for
%    biped walking model. (x is of dimension 6)

[r,m,L,g]=model_params_two_link;

q_sw=x(2); q_st=x(1); 

% De matrix
De=zeros(4,4);
De(1,1)=(5*m*r^2)/4;
De(1,2)=-(m*r^2*cos(q_st - q_sw))/2;
De(1,3)=(3*m*r*cos(q_st))/2;
De(1,4)=-(3*m*r*sin(q_st))/2;
De(2,1)=-(m*r^2*cos(q_st - q_sw))/2;
De(2,2)=(m*r^2)/4;
De(2,3)=-(m*r*cos(q_sw))/2;
De(2,4)=(m*r*sin(q_sw))/2;
De(3,1)=(3*m*r*cos(q_st))/2;
De(3,2)=-(m*r*cos(q_sw))/2;
De(3,3)=2*m;
De(4,1)=-(3*m*r*sin(q_st))/2;
De(4,2)=(m*r*sin(q_sw))/2;
De(4,4)=2*m;

% E matrix
E=zeros(2,4);
E(1,1)=r*cos(q_st);
E(1,2)=-r*cos(q_sw);
E(1,3)=1;
E(2,1)=-r*sin(q_st);
E(2,2)=r*sin(q_sw);
E(2,4)=1;
tmp_vec=inv([De -E';E zeros(2)])*[De*[x(3:4)';zeros(2,1)];zeros(2,1)];

x_new(1)=x(2);
x_new(2)=x(1);
x_new(3)=tmp_vec(2);
x_new(4)=tmp_vec(1);
F_ext=tmp_vec(5:6);
