function p2f = swingFoot_pos( qF ,p)

 q_st = qF(1); q_sw = qF(2);
 
 p_Mh = p.r * [sin(q_st); cos(q_st)];
 
 p2f = p_Mh - p.r *[sin(q_sw);cos(q_sw)];
 
end
