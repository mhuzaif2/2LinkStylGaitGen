

% Absolute Coordinates based modeling of the planar biped with partial
% actuation.

clear all
close all
clc

dbstop if error

filename = strrep(mfilename,'gen_model','model_save');

if isempty(dir([filename,'.mat']))
	clear
	filename = strrep(mfilename,'gen_model','model_save');

	%% variable declarations
	%
	syms q_sw q_st z1 z2 real
	syms dq_sw dq_st dz1 dz2 real
	syms m g L r real
	syms q_st_d real

	%% generalized coordinates
	%
	q=[q_st;q_sw];
	qe=[q;z1;z2];

	%% first derivative of generalized coordinates
	%
	dq=[dq_st;dq_sw];
	dqe=[dq;dz1;dz2];

	%% Generate De matrix.  To do so, we must go through the trouble of
	%% defining the positions of the masses using the augmented
	%% configuration variables.
	%

	%% position of masses in system
	%
    p_m1 = [z1+r/2*sin(q_st); z2+r/2*cos(q_st)];  % Position of the center of swing leg
	p_Mh = [z1+r*sin(q_st); z2+r*cos(q_st)];      % position of the hip joint
	p_m2 = p_Mh-[r/2*sin(q_sw); r/2*cos(q_sw)];   % Position of the center of stance leg

	%% velocities of masses in system
	%
	v_m1=jacobian(p_m1,qe)*dqe;
	v_Mh=jacobian(p_Mh,qe)*dqe;
	v_m2=jacobian(p_m2,qe)*dqe;

	%% kinetic energy of masses in system
	%
	KE_m1=simplify(m/2*v_m1'*v_m1);
	KE_m2=simplify(m/2*v_m2'*v_m2);

	%% total kinetic energy of system
	%
	KE=KE_m1+KE_m2;

	%% potential energy of masses in system
	%
	PE_m1=p_m1(2)*m*g;

	PE_m2=p_m2(2)*m*g;

	%% total potential energy of system
	%
	PE = PE_m1 + PE_m2;

	De=simplify(jacobian(jacobian(KE,dqe).',dqe));

	N=max(size(qe));
    syms Ce
	for k=1:N,
		for j=1:N,
			Ce(k,j)=0*g;
			for i=1:N,
				Ce(k,j)=Ce(k,j)+1/2*(diff(De(k,j),qe(i))+...
					diff(De(k,i),qe(j))-...
					diff(De(i,j),qe(k)))*dqe(i);
			end
		end
	end

	Ge=jacobian(PE,qe).';

	%% Now that We're done generating the De matrix we now can use the
	%% non-augmented configuration variables to derive the required
	%% matrices.
	%

	%% position of masses in system
	%
    p_m1=[r/2*sin(q_st); r/2*cos(q_st)];
	p_m2=p_Mh-[r/2*sin(q_sw); r/2*cos(q_sw)];


	%% velocities of masses in system
	%
	v_m1=jacobian(p_m1,q)*dq;
	v_Mh=jacobian(p_Mh,q)*dq;
	v_m2=jacobian(p_m2,q)*dq;

	%% kinetic energy of masses in system
	%
	KE_m1=simplify(m/2*v_m1'*v_m1);
	KE_m2=simplify(m/2*v_m2'*v_m2);

	%% total kinetic energy of system
	%
	KE = KE_m1 + KE_m2;

	%% potential energy of masses in system
	%
	PE_m1=p_m1(2)*m*g;
	PE_m2=p_m2(2)*m*g;

	%% total potential energy of system
	%
	PE = PE_m1 + PE_m2;

	%% the Lagragian
	%
	%Lag=KE-PE;

	%% Form D, C, G, B, and F matrices of
	%%
	%% D(q)ddq+C(q,dq)dq+G(q)=B*tau
	%%
	%% where tau=[tau1; tau2]
	%
	D = jacobian(jacobian(KE,dq).',dq);

	N = max(size(q));
    syms C
	for k=1:N,
		for j=1:N,
			C(k,j)=0*g;
			for i=1:N,
				C(k,j)=C(k,j)+1/2*(diff(D(k,j),q(i))+...
					diff(D(k,i),q(j))-...
					diff(D(i,j),q(k)))*dq(i);
			end
		end
	end

	G=jacobian(PE,q).';


    B = [0 1]';
	Psi=[z1 + r*sin(q_st)-r*sin(q_sw);
		z2 + r*cos(q_st)-r*cos(q_sw)];

	%% Used to calculate post-impact conditions
	%
	E=jacobian(Psi,qe);


else
	clear
	load(strrep(mfilename,'gen_model','model_save'));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Output functions for use in simulation
%%
%

N=max(size(q));

%% First, output model to a file called
%%
%%    dynamics+<model type>.m
%%
%

%% Output file header
%
fcn_name=strrep(mfilename,'gen_model','EOM');
fid=fopen([fcn_name,'.m'],'w');
fprintf(fid,'function [D,C,G,B,H]=%s',fcn_name);
fprintf(fid,'(x,a)\n');
fprintf(fid,'%% %s    Model of two-link biped walker model.\n',...
	upper(fcn_name));
fprintf(fid,'%%    [D,C,G,B] = %s',upper(fcn_name));
fprintf(fid,'(X,\n%%      A) is the two-link\n');
fprintf(fid,'%%    biped walking model. (x is of dimension %s)\n\n',...
	num2str(2*N));

%% Read in constants
%
fprintf(fid,'[r,m,L,g]=model_params_two_link;\n\n');

%% Reassign configuration parameters
%
fprintf(fid,'q_sw=x(2); q_st=x(1);\n');
fprintf(fid,'dq_sw=x(4); dq_st=x(3);\n\n');

%% Model output
%
fprintf(fid,'%% D matrix\n');
fprintf(fid,'D=zeros(%s);\n',num2str(N));
for k=1:N,
	for j=1:N,
		if D(k,j)~=0
			ttt=char(D(k,j));
			fprintf(fid,'D(%s,%s)=%s;\n',num2str(k),num2str(j),ttt);
		end
	end
end

fprintf(fid,'\n%% C matrix\n');
fprintf(fid,'C=zeros(%s);\n',num2str(N));
for k=1:N,
	for j=1:N,
		if C(k,j)~=0
			ttt=char(C(k,j));
			fprintf(fid,'C(%s,%s)=%s;\n',num2str(k),num2str(j),ttt);
		end
	end
end

fprintf(fid,'\n%% G matrix\n');
fprintf(fid,'G=zeros(%s,1);\n',num2str(N));
for k=1:N,
	if G(k)~=0
		ttt=char(G(k));
		fprintf(fid,'G(%s)=%s;\n',num2str(k),ttt);
	end
end

fprintf(fid,'\n%% B matrix\n');
[N,M]=size(B);
fprintf(fid,'B=zeros(%s,%s);\n',num2str(N),num2str(M));
for k=1:N,
	for j=1:M,
		if B(k,j)~=0
			ttt=char(B(k,j));
			fprintf(fid,'B(%s,%s)=%d;\n',num2str(k),num2str(j),ttt);
		end
	end
end
fclose(fid);



N=max(size(q));
%% Swing to Stance Leg Transition
% stance_force_+<model type>.m
%%%

fcn_name=strrep(mfilename,'gen_model','transition');
fid=fopen([fcn_name,'.m'],'w');
fprintf(fid,'function [x_new,F_ext]=%s(x)\n',fcn_name);
fprintf(fid,'%% %s    Calculate the state of the system after impact.\n',...
	upper(fcn_name));
fprintf(fid,'%%          (Last two entries are the forces at the toe.\n');
fprintf(fid,'%%    [x_new, F_ext] = %s(X) is the transition function for\n',...
	upper(fcn_name));
fprintf(fid,'%%    biped walking model. (x is of dimension %s)\n\n',...
	num2str(2*N+2));
N=max(size(qe));

fprintf(fid,'[r,m,L,g]=model_params_two_link;\n\n');

fprintf(fid,'q_sw=x(2); q_st=x(1); \n\n');

fprintf(fid,'%% De matrix\n');
fprintf(fid,'De=zeros(%s,%s);\n',num2str(N),num2str(N));
for k=1:N,
	for j=1:N,
		if De(k,j)~=0
			ttt=char(De(k,j));
			fprintf(fid,'De(%s,%s)=%s;\n',num2str(k),num2str(j),ttt);
		end
	end
end

[N,M]=size(E);
fprintf(fid,'\n%% E matrix\n');
fprintf(fid,'E=zeros(%s,%s);\n',num2str(N),num2str(M));
for k=1:N,
	for j=1:M,
		if E(k,j)~=0
			ttt=char(E(k,j));
			fprintf(fid,'E(%s,%s)=%s;\n',num2str(k),num2str(j),ttt);
		end
	end
end

fprintf(fid,...
	'tmp_vec=inv([De -E'';E zeros(2)])*[De*[x(3:4)'';zeros(2,1)];zeros(2,1)];\n\n');

fprintf(fid,'x_new(1)=x(2);\n');
fprintf(fid,'x_new(2)=x(1);\n');
fprintf(fid,'x_new(3)=tmp_vec(2);\n');
fprintf(fid,'x_new(4)=tmp_vec(1);\n');
fprintf(fid,'F_ext=tmp_vec(5:6);\n');
fclose(fid);

%% stance-leg force calculation routine (for impacts) to a file called
%%
%%    stance_force_+<model type>.m
%%
%

N=max(size(q));
Ne=max(size(qe));

%% Output file header
%
fcn_name=strrep(mfilename,'gen_model','stance_force');
fid=fopen([fcn_name,'.m'],'w');
fprintf(fid,'function [f_tan,f_norm]=%s(x,dx,u)\n',fcn_name);
fprintf(fid,'%% %s    Calculate the forces on the stance\n',...
	upper(fcn_name));
fprintf(fid,'%%                            leg during impact.\n');
fprintf(fid,'%%    [F_TAN,F_NORM] = %s(X,DX,U) are the forces on the\n',...
	upper(fcn_name));
fprintf(fid,'%%    stance leg at impact.\n\n');

% Read in constants
%
fprintf(fid,'[r,m,L,g]=model_params_two_link;\n\n');

%% Reassign configuration parameters
%
fprintf(fid,'q_sw=x(2); q_st=x(1); \n');
fprintf(fid,'dq_sw=x(4); dq_st=x(3); \n\n');

%% Model output
%
fprintf(fid,'%% De11 matrix\n');
fprintf(fid,'De11=zeros(%s,%s);\n',num2str(N),num2str(N));
for k=1:N,
	for j=1:N,
		if De(k,j)~=0
			ttt=char(De(k,j));
			fprintf(fid,'De11(%s,%s)=%s;\n',num2str(k),num2str(j),ttt);
		end
	end
end

fprintf(fid,'\n%% De12 matrix\n');
fprintf(fid,'De12=zeros(%s,%s);\n',num2str(N),num2str(Ne-N));
for k=1:N,
	for j=1:Ne-N,
		if De(k,j+N)~=0
			ttt=char(De(k,j+N));
			fprintf(fid,'De12(%s,%s)=%s;\n',num2str(k),num2str(j),ttt);
		end
	end
end

fprintf(fid,'\n%% De22 matrix\n');
fprintf(fid,'De22=zeros(%s,%s);\n',num2str(Ne-N),num2str(Ne-N));
for k=1:Ne-N,
	for j=1:Ne-N,
		if De(k+N,j+N)~=0
			ttt=char(De(k+N,j+N));
			fprintf(fid,'De22(%s,%s)=%s;\n',num2str(k),num2str(j),ttt);
		end
	end
end

fprintf(fid,'\n%% Ce11 matrix\n');
fprintf(fid,'Ce11=zeros(%s,%s);\n',num2str(N),num2str(N));
for k=1:N,
	for j=1:N,
		if Ce(k,j)~=0
			ttt=char(Ce(k,j));
			fprintf(fid,'Ce11(%s,%s)=%s;\n',num2str(k),num2str(j),ttt);
		end
	end
end

fprintf(fid,'\n%% Ce21 matrix\n');
fprintf(fid,'Ce21=zeros(%s,%s);\n',num2str(Ne-N),num2str(N));
for k=1:Ne-N,
	for j=1:N,
		if Ce(k+N,j)~=0
			ttt=char(Ce(k+N,j));
			fprintf(fid,'Ce21(%s,%s)=%s;\n',num2str(k),num2str(j),ttt);
		end
	end
end

fprintf(fid,'\n%% Ge1 matrix\n');
fprintf(fid,'Ge1=zeros(%s,%s);\n',num2str(N),num2str(1));
for k=1:N,
	if Ge(k)~=0
		ttt=char(Ge(k));
		fprintf(fid,'Ge1(%s,%s)=%s;\n',num2str(k),num2str(1),ttt);
	end
end

fprintf(fid,'\n%% Ge2 matrix\n');
fprintf(fid,'Ge2=zeros(%s,%s);\n',num2str(Ne-N),num2str(1));
for k=1:Ne-N,
	if Ge(k+N)~=0
		ttt=char(Ge(k+N));
		fprintf(fid,'Ge2(%s,%s)=%s;\n',num2str(k),num2str(1),ttt);
	end
end

fprintf(fid,'\n%% B matrix\n');
[N,M]=size(B);
fprintf(fid,'B=zeros(%s,%s);\n',num2str(N),num2str(M));
for k=1:N,
	for j=1:M,
		if B(k,j)~=0
			ttt=char(B(k,j));
			fprintf(fid,'B(%s,%s)=%s;\n',num2str(k),num2str(j),ttt);
		end
	end
end

fprintf(fid,'DD=inv((De12*inv(De22)).''*De12*inv(De22))*(De12*inv(De22)).'';\n');

fprintf(fid,'F=DD*(-(De11-De12*inv(De22)*De12.'')...\n');
fprintf(fid,'  *dx(3:4)+(De12*inv(De22)*Ce21-Ce11)...\n');
fprintf(fid,'  *dx(1:2)+De12*inv(De22)*Ge2-Ge1+B*u);\n\n');

fprintf(fid,'f_tan=F(1);\n');
fprintf(fid,'f_norm=F(2);\n');
fclose(fid);
