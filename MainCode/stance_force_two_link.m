function [f_tan,f_norm]=stance_force_two_link(x,dx,u)
% STANCE_FORCE_TWO_LINK    Calculate the forces on the stance
%                            leg during impact.
%    [F_TAN,F_NORM] = STANCE_FORCE_TWO_LINK(X,DX,U) are the forces on the
%    stance leg at impact.

[r,m,L,g]=model_params_two_link;

q_sw=x(2); q_st=x(1); 
dq_sw=x(4); dq_st=x(3); 

% De11 matrix
De11=zeros(2,2);
De11(1,1)=(5*m*r^2)/4;
De11(1,2)=-(m*r^2*cos(q_st - q_sw))/2;
De11(2,1)=-(m*r^2*cos(q_st - q_sw))/2;
De11(2,2)=(m*r^2)/4;

% De12 matrix
De12=zeros(2,2);
De12(1,1)=(3*m*r*cos(q_st))/2;
De12(1,2)=-(3*m*r*sin(q_st))/2;
De12(2,1)=-(m*r*cos(q_sw))/2;
De12(2,2)=(m*r*sin(q_sw))/2;

% De22 matrix
De22=zeros(2,2);
De22(1,1)=2*m;
De22(2,2)=2*m;

% Ce11 matrix
Ce11=zeros(2,2);
Ce11(1,2)=-(dq_sw*m*r^2*sin(q_st - q_sw))/2;
Ce11(2,1)=(dq_st*m*r^2*sin(q_st - q_sw))/2;

% Ce21 matrix
Ce21=zeros(2,2);
Ce21(1,1)=-(3*dq_st*m*r*sin(q_st))/2;
Ce21(1,2)=(dq_sw*m*r*sin(q_sw))/2;
Ce21(2,1)=-(3*dq_st*m*r*cos(q_st))/2;
Ce21(2,2)=(dq_sw*m*r*cos(q_sw))/2;

% Ge1 matrix
Ge1=zeros(2,1);
Ge1(1,1)=-(3*g*m*r*sin(q_st))/2;
Ge1(2,1)=(g*m*r*sin(q_sw))/2;

% Ge2 matrix
Ge2=zeros(2,1);
Ge2(2,1)=2*g*m;

% B matrix
B=zeros(2,1);
B(2,1)=;
DD=inv((De12*inv(De22)).'*De12*inv(De22))*(De12*inv(De22)).';
F=DD*(-(De11-De12*inv(De22)*De12.')...
  *dx(3:4)+(De12*inv(De22)*Ce21-Ce11)...
  *dx(1:2)+De12*inv(De22)*Ge2-Ge1+B*u);

f_tan=F(1);
f_norm=F(2);
