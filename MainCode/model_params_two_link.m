function [r,m,L,g]=model_params_two_link
	%   Model parameters for three-link legged biped.
	% Eric Westervelt
	% 20 February 2007
	r= 1 ;   % length of a leg
	m=5 ;   % mass of a leg
	Mh=0; % mass of hips
	Mt=10; % mass of torso    
	L=0; % distance between hips and torso
	g=9.8; % acceleration due to gravity
