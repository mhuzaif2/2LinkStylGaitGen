
function ddq =  dynamics(x, u)

ddq = [];
s = size(x,2);

    for i= 1:s
          ddq = [ddq dyn_wrap(x(:,i),u(:,i))];
    end
end

function dx = dyn_wrap(x, u)


[D,C,G,B] = EOM_two_link(x(1:4));

Fx = D \ (-C*x(3:4)-G);
Gx = D\ B;

dx(1:2) = x(3:4);
dx(3:4) = Fx + Gx * u;
dx = dx';
end

function [D,C,G,B,H]=EOM_two_link(x)
% EOM_TWO_LINK    Model of two-link biped walker model.
%    [D,C,G,B] = EOM_TWO_LINK(X,
%      A) is the two-link
%    biped walking model. (x is of dimension 4)

[r,m,L,g]=model_params_two_link;

q_sw=x(2); q_st=x(1);
dq_sw=x(4); dq_st=x(3);

% D matrix
D=zeros(2);
D(1,1)=(m*r^2)/4 + m*r^2*cos(q_st)^2 + m*r^2*sin(q_st)^2;
D(1,2)=- (m*r^2*cos(q_st)*cos(q_sw))/2 - (m*r^2*sin(q_st)*sin(q_sw))/2;
D(2,1)=- (m*r^2*cos(q_st)*cos(q_sw))/2 - (m*r^2*sin(q_st)*sin(q_sw))/2;
D(2,2)=(m*r^2*cos(q_sw)^2)/4 + (m*r^2*sin(q_sw)^2)/4;

% C matrix
C=zeros(2);
C(1,2)=dq_sw*((m*r^2*cos(q_st)*sin(q_sw))/2 - (m*r^2*cos(q_sw)*sin(q_st))/2);
C(2,1)=-dq_st*((m*r^2*cos(q_st)*sin(q_sw))/2 - (m*r^2*cos(q_sw)*sin(q_st))/2);

% G matrix
G=zeros(2,1);
G(1)=-(3*g*m*r*sin(q_st))/2;
G(2)=(g*m*r*sin(q_sw))/2;

% B matrix
B=zeros(2,1);
B(2,1)=1;
end