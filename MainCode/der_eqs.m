% Writes script files for the following
% 1- computing the contact forces on the stance leg. 
% 2- finding the COM of the robot during the walk.


    syms q_sw q_st z1 z2 real
    syms dq_sw dq_st dz1 dz2 real
    syms ddq_st ddq_sw real
    syms m Mh Mt g L r real
    syms q_st_d real
    syms Ft Fn real
    syms u1 u2 t real


    q=[q_st;q_sw];
    qe=[q;z1;z2];

    %%% first derivative of generalized coordinates

    dq  = [dq_st; dq_sw];
    ddq  = [ddq_st; ddq_sw];
    dqe = [dq;dz1;dz2];


    i = sym([1;0]);   %Horizontal axis
    j = sym([0;1]);   %Vertical axis

    u = [u1];   

    % Neat trick to compute derivatives using the chain rule
    derivative = @(in)( jacobian(in,[q;dq])*[dq;ddq] );

    % Unite reference vectors

    e1 = sin(q_st) * i + cos(q_st) * j;
    e2 = e1;
    e3 = - sin(q_sw) * i - cos(q_sw) * j;
    e4 = i;

    % Position vectors

    P0 = 0 * i + 0 * j;
    P1 = P0 + r/2 * e1;
    P2 = P1 + r/2 * e1;
    P3 = P2 + r/2 * e3;

    % Weight vectors

    w1 = - m * g * j; w2 = - m * g * j; 

    % Center of Mass definition

    G = (m * P1 + m * P3) / ( 2*m );

    dG = derivative(G);

    ddG = derivative(dG);
    ddGx = ddG(1); ddGy = ddG(2);
    % Contact Force Calculations

    eqnForce3 = w1 + w2 + Ft * i + Fn * j;
    eqnInertia3 = ( 2*m ) * ddG;
    [ AA, bb ] = equationsToMatrix( eqnForce3 - eqnInertia3, [Ft ; Fn] );
    ContactForces = AA \ bb;

    matlabFunction(ContactForces(1),ContactForces(2),...
            'file','ctctForces_wrap.m',...
            'vars',{...
            'q_st','q_sw',...
            'dq_st','dq_sw',...
            'ddq_st','ddq_sw',...
            'm',...
            'r',...
            'g'},...
            'outputs',{'Fx','Fy'});

    matlabFunction( ddG,'file','COM_accel.m',...
            'vars',{...
            'q_st','q_sw',...
            'dq_st','dq_sw',...
            'ddq_st','ddq_sw',...
            'm',...
            'r',...
            'g'},...
            'outputs',{'ddG'})

    matlabFunction( G,'file','pos_com.m',...
            'vars',{...
            'q_st','q_sw',...            
            'm',...
            'r'
            },...
            'outputs',{'G'})
        
  