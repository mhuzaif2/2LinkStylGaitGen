function dec = bin_to_dec (array)
dec = 0;
    for i = 1: size(array,2)
       dec = dec + array(end - (i - 1))*2^(i-1);        
    end

end