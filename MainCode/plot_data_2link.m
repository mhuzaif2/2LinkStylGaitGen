function plot_data_2link(t, q, dq, u, F, handles)
    %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%
    %                     Plot the solution                                   %
    %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~%    
    if (~exist('handles','var') | ~exist('F','var'))                       
        % In case no arguments are passed
            if (~exist('x','var'))
                load('data.mat');
                q = x(1:2,:); dq = x(3:4,:);
                [Fx_final, Fy_final] = contactForces(t,x,u,param);       
                F = [Fx_final Fy_final];        
            end
        figure('Color','white')
        subplot(221)
        plot(t,q(1,:),':','LineWidth',4)
        hold on
        plot(t,q(2,:),'-','LineWidth',4)    
        axis auto
        legend('q_{st}','q_{sw}');
        xlabel('time')
        ylabel('Joint Positions')
        set(gca,'FontSize',22)
        grid on
        
        subplot(222)
        plot(t,dq(1,:),':','LineWidth',4)
        hold on
        plot(t,dq(2,:),'-','LineWidth',4)    
        axis auto
        h = legend('$\dot{q}_{st}$','$\dot{q}_{sw}$');
        set(h,'Interpreter','latex');
        xlabel('time')
        ylabel('Joint Velocities')
        set(gca,'FontSize',22)
        grid on
        
        subplot(223)
        plot(t,u(1,:),'LineWidth',4)
        axis auto
        xlabel('time')
        ylabel('Control Input')
        legend('u_1');
        set(gca,'FontSize',22)
        grid on

        subplot(224)
         plot(t, F(:,1),':','LineWidth',4)
        hold on
        plot(t, F(:,2),'-','LineWidth',4)
        axis auto
        legend('F^{st}_T','F^{st}_N')
        xlabel('time')
        ylabel({'Ground Reaction Forces','on Stance Foot'})
        set(gca,'FontSize',22)
        grid on
        set(gcf,'color','white')
        title('Resulting Plots for One Walking Step')
    else
        axes(handles.axes9)
        cla
        plot(t,q(1,:),':','LineWidth',4)
        hold on
        plot(t,q(2,:),'-','LineWidth',4)    
        axis auto
        legend('q_{st}','q_{sw}');
        xlabel('time')
        ylabel('Joint Positions')
        set(gca,'FontSize',22)
        grid on
        axes(handles.axes10)
        cla
        plot(t,dq(1,:),':','LineWidth',4)
        hold on
        plot(t,dq(2,:),'-','LineWidth',4)    
        axis auto
        h = legend('$\dot{q}_{st}$','$\dot{q}_{sw}$');
        set(h,'Interpreter','latex');
        xlabel('time')
        ylabel('Joint Velocities')
        set(gca,'FontSize',22)
        grid on
        axes(handles.axes11)
        cla
        plot(t,u(1,:),':','LineWidth',4)
        axis auto
        xlabel('time')
        ylabel('Control Input')
        legend('u_1');
        set(gca,'FontSize',22)
        grid on
        
        axes(handles.axes12)
        cla
        plot(t, F(:,1),':','LineWidth',4)
        hold on
        plot(t, F(:,2),'-','LineWidth',4)
        axis auto
        legend('F^{st}_T','F^{st}_N')
        xlabel('time')
        ylabel({'Ground Reaction Forces','on Stance Foot'})
        set(gca,'FontSize',22)
        grid on
        set(gcf,'color','white')
        
        title('Resulting Plots for One Walking Step')
    end

