function [c, ceq] = pathConstraint_GUI(t, x, u, param)
% This function computes the Path Constraints in optimization

% INPUTS:
%   t = time vector for the trajectory
%   x = state vector for the trajectory
%   u = input vector for the trajectory
%   param = parameters for the model

% OUTPUTS:
%   ceq = equality constraint
%   ceqGrad = gradient of equality constraints
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compute the constraint forces on the stance leg foot at every time step
  [Fx, Fy] = contactForces(t, x, u, param);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Friction cone constraint
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  c1 = Fx - param.mu * Fy;
  c2 = -(Fx + param.mu * Fy);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Normal GRF constraint
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  c3 = -Fy';

  c4 = [];
  c = [c1'; c2'; c3; c4];
  ceq = [];
end
