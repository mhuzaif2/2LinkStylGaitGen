% anim_2link.m
% A script that animates the biped model leaving behind its snapshots at
% different instants in time. Compare with motion_shots.m

% Umer Huzaifa
% RAD Lab UIUC

function anim_2link(options, tstep)   
    dbstop if error
    load('data.mat');
    nstep = 1.0;

    x_final = x;
    nTime = size(x,2);

    for i = 1:nstep
       if nstep >1
            x_final = [x_final [transition_two_link(x_final(:,end)')'] x_final(:,2:(nTime))];
       end
    end
    if nstep >1
        t = linspace(0, nstep, (nstep + 1)*nTime);
    else
        t = linspace(0, nstep, (nstep) * nTime);
    end
    switch nargin
        case 0
          options={'ColorScheme',2}; % Red and Blue colors for the legs
          tstep = 10;
    end
      anim(t,x_final,tstep,options)

function x_f =  stitch_steps(x, n) % An unfinished try in restricting the legs from switching
    x_f = x;
    for i = 1: n
         x_pos = [0 1 0 zeros(1,3);1 0 0 zeros(1,3);0 0 1 zeros(1,3);...
              zeros(1,3) 1 0 0; zeros(1,3) 0 1 0; zeros(1,3) 0 0 1] *...
              transition_two_link(x_f(:,end)')';
         x_temp = [x_pos ...
           [x(2,2:(end)); x(1,2:(end)); x(3,2:(end));...
            x(4,2:(end)); x(5,2:(end)); x(6,2:(end))]];
    end

function anim(t, x, tstep, options)
    
    [n, m] = size(x');
    %%% For nstep = 1
    % A deterministic approach to finding horizontal position of hip
    pH = zeros(n,2);
    [r, m, L, g] = model_params_two_link;
    q = x(1:2,:); q_st = q(1,:);
    for j=1:n
		  pH(j,:) = [r*sin(q_st(j)) r*cos(q_st(j))];
    end
    pH_horiz = pH(:,1)';
    
    cla
	anim_axis = [-1 1 0 4];
	axis(anim_axis)
    set(gca,'ycolor','None')
	  % Use actual relations between masses in animation
  	scl = 0.04; % factor to scale masses
  	mr_legs = m^(1/3)*scl; % radius of mass for legs
  	ground_color = 'k'; % a.k.a. black
  	% Approximate circular shape of mass
  	param = linspace(0,2*pi+2*pi/50,50);
  	xmass_legs = mr_legs*cos(param); % Definition of the circle
  	ymass_legs = mr_legs*sin(param); % representing legs COM
    % Color Choices for Legs
    switch options{2}
        case 1
            leg1_color = [0.5,0.5,0.5];
            leg2_color = [0.5,0.5,0.5];
        case 2
            leg1_color = [0,0,1];
            leg2_color = [1,0,0];
    end

	% Draw ground
  	buffer = 5;
  	ground = line([-buffer pH_horiz(n)+buffer],[0 0]);
  	set(ground,'Color',ground_color,'LineWidth',2);
    hold on
    pF1 = []; pF2 = [];  
    for k = 1:n
	  [pFoot1,pFoot2] = limb_position(q(:,k),pH(k,:));
      pF1 = [pF1; pFoot1'];
      pF2 = [pF2; pFoot2'];
    end
    for k = 1:tstep:n
      % Draw leg one
      leg1 = line([pF1(k,1) pH(k,1)],[pF1(k,2) pH(k,2)],'LineStyle','-','LineWidth',2);
      mass1 = patch(xmass_legs+(pH(k,1)-pF1(k,1))/2,...
                ymass_legs+(pH(k,2)-pF1(k,2))/2,leg1_color);
      % Draw leg two
      leg2 = line([pF2(k,1) pH(k,1)],[pF2(k,2) pH(k,2)],'LineStyle','-','LineWidth',2,'Color','red');
      uistack(leg2);
      mass2 = patch(xmass_legs+pH(k,1)-(pH(k,1)-pF2(k,1))/2,...
                ymass_legs+pH(k,2)-(pH(k,2)-pF2(k,2))/2,leg2_color);
      uistack2(mass1,'bottom',2);
      uistack2(leg2,'bottom',3);
      uistack2(leg1,'bottom',4);
	  pause(1/20);
    end
% a function to calculate the limb position
function [pFoot1,pFoot2] = limb_position(q,pH)
  % Use position of hips as location of stance leg foot.
  [r,m,L,g]=model_params_two_link;
  pFoot1=[0; 0];
  pFoot2=[pH(1)-r*sin(q(2)); pH(2)-r*cos(q(2))];
