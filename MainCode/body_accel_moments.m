function [ax, ay] =  body_accel_moments(t, x, u, param, Fx_final, Fy_final)

% The code for computing Body Accelerations and Moments as the forces and
% moments balance is satisfied.

% Umer Huzaifa
% RADLab UIUC
% October 12th 2017


ax = (- Fx_final)/ (2 * param.m);

ay = (2 * param.m * param.g - Fy_final)/ ...
    (2 * param.m );

