function varargout = GUI_interface(varargin)

    gui_Singleton = 1;
    gui_State = struct('gui_Name',       mfilename, ...
                       'gui_Singleton',  gui_Singleton, ...
                       'gui_OpeningFcn', @GUI_OpeningFcn, ...
                       'gui_OutputFcn',  @GUI_OutputFcn, ...
                       'gui_LayoutFcn',  [] , ...
                       'gui_Callback',   []);
    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)

    % Choose default command line output for GUI
    handles.output = hObject;
    handles.tabManager = TabManager( hObject );
   
    % Update handles structure
    guidata(hObject, handles);
    
    % To Display all the related images on GUI
    Disp_images(hObject, eventdata, handles)       
        
    
function Disp_images(hObject, eventdata, handles)       
       
    % Add the logos of RADLab and UIUC
    
    axes(handles.axes18);
    mim = imread('I_logo.png');
    imshow(mim);    
    axis off
    box off
    axis image;    
    axes(handles.axes17);
    mim = imread('radlab_logo.png');
    imshow(mim);
    axis off
    box off
    axis image;
    
    % Add picture for the biped diagram    
    axes(handles.axes20);
    mim = imread('TL_walker.png');
    imshow(mim);    
    axis off
    box off
    axis image;
    
    % Add text for the cost function options     
    h = handles.axes23;
    axes(handles.axes23);
    mim = imread('const_100.png');
    imshow(mim);    
    axis off
    box off
    axis image;
    
    % minE
    h = handles.axes22;
    axes(handles.axes22);
    mim = imread('minE.png');
    imshow(mim);
    axis off
    box off
    axis image;
    
    % qsw
    h = handles.axes24;
    axes(handles.axes24);
    mim = imread('qsw.png');
    imshow(mim);
    axis off
    box off
    axis image;

    % dqsw2
    h = handles.axes25;
    axes(handles.axes25);
    mim = imread('dqsw2.png');
    imshow(mim);
    axis off
    box off
    axis image;    
    
% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles) 

    varargout{1} = handles.output;

% --- Executes on button press 'Run'
function Run_Callback(hObject, eventdata, handles)
       
    gait_param.TL =  get(handles.TL_v,'Value');  % Selecting the Step Length 
    
    cost_find = [get(handles.minE,'Value') get(handles.qsw,'Value') ...
                 get(handles.dqsw2,'Value') get(handles.const,'Value')];    
              
    gait_param.sT = 1;    % Step Time
     % Animation parameters
     % Fs -- Sampling frequency 
     % nstep -- Number of walking steps in animation
    gait_param.Fs = 20; gait_param.nstep = 5; 
    handles.Fs = gait_param.Fs; handles.nstep = gait_param.nstep; 
    
    switch(bin_to_dec(cost_find))
        case 8
            gait_param.cost = 'minE';
        case 4
            gait_param.cost = 'qsw';
        case 2
            gait_param.cost = 'dqsw2';
        case 1
            gait_param.cost = 'const';       
    end
     
    optim_dcol(gait_param, handles);
    guidata(hObject, handles);
    
function TL_v_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function TL_v_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on Replay Button Press.
function Replay_Callback(hObject, eventdata, handles)
   axes(handles.axes13)
   hold on       
   animate({'ColorScheme',2}, handles.Fs, handles.nstep)  
   axes(handles.axes21)       
   motion_shots_2link({'ColorScheme',2}, handles.Fs)
